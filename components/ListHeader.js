import React, { useEffect, useState } from "react";
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Dimensions
} from "react-native";

const listHeader = () => {
  const HEADER_HEIGHT = 42;

  return (
    <View>
      <View
        style={{
          height: HEADER_HEIGHT,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          paddingHorizontal: 6,

          backgroundColor: "#eee"
        }}
      >
        <View>
          <Text style={{ fontSize: 24, fontWeight: "500" }}>Navigation</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Image
            style={{
              height: HEADER_HEIGHT - 12,
              width: HEADER_HEIGHT - 12,
              margin: 5
            }}
            source={{
              uri: "https://img.icons8.com/?size=512&id=37784&format=png"
            }}
          />
          <Image
            style={{
              height: HEADER_HEIGHT - 12,
              width: HEADER_HEIGHT - 12,
              margin: 5
            }}
            source={{
              uri: "https://img.icons8.com/?size=512&id=59832&format=png"
            }}
          />
        </View>
      </View>
      <View>
        <Text>Account Info</Text>
      </View>
      <View>
        <Text>About account</Text>
      </View>
      <View>
        <Text>Buttons</Text>
      </View>
      <View>
        <Text>Flatlist</Text>
      </View>
    </View>
  );
};

export default listHeader;