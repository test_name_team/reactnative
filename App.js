import React, { useState } from "react";
import {
  Alert,
  Button,
  ActivityIndicator,
  Image,
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Switch,
} from "react-native";

import Calculator from "./components/Calculator";
import Registration from "./components/Registration";
import Gallery from "./components/Gallery";
import GalleryM from "./components/GalleryM";
import GalleryLesson from "./components/GalleryLesson";
import GalleryMini from "./components/GalleryMini";

const App = () => {
  return (
    <View style={styles.app}>
      {/* <ScrollView contentContainerStyle={styles.scrollDiv}> */}
      <GalleryMini />
      {/* <GalleryLesson /> */}
      {/* <GalleryM /> */}
      {/* <Gallery /> */}
      {/* <Registration /> */}
      {/* <Calculator /> */}
      {/* </ScrollView> */}
    </View>
  );
};

const styles = StyleSheet.create({
  app: {
    flex: 1,
  },
  scrollDiv: {
    flex: 1,
  },
});

export default App;
